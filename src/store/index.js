import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    surveyState: false,
    answers: [],
    questions: [],
    questionIndex: 0,
    surveyEnd: false
  },
  getters: {
    surveyState: state => state.surveyState,
    answers: state => state.answers,
    questions: state => state.questions,
    questionIndex: state => state.questionIndex,
    surveyEnd: state => state.surveyEnd
  },
  mutations: {
    updateQuestions: (state, value) => {
      Vue.set(state, "questions", value);
    },
    updateSurvey: (state, value) => {
      Vue.set(state, "surveyState", value);
    },
    resetState: state => {
      Vue.set(state, "answers", []);
      Vue.set(state, "surveyState", false);
      Vue.set(state, "surveyEnd", false);
    },
    nextQuestion: state => {
      console.log(state.answers);
      let lastAnswer = state.answers[state.answers.length - 1].answer;
      let index = state.questionIndex + 1;
      let total = Object.keys(state.questions).length;
      for (; index < total; index++) {
        if (
          state.questions[index].requirement !== undefined &&
          lastAnswer.indexOf(state.questions[index].requirement) != -1
        ) {
          Vue.set(state, "questionIndex", index);
          break;
        } else if (state.questions[index].requirement == undefined) {
          Vue.set(state, "questionIndex", index);
          break;
        }
      }
      if (index >= state.questions.length) {
        Vue.set(state, "surveyEnd", true);
      }
    },
    saveAnswer(state, values) {
      state.answers.push(values);
    }
  },
  actions: {
    save: (context, values) => {
      context.commit("saveAnswer", values);
    }
  },
  modules: {}
});
